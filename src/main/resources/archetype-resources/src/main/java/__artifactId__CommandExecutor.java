package ${package};

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public class ${artifactId}CommandExecutor implements CommandExecutor{

    private final Plugin plugin;

    public ${artifactId}CommandExecutor(Plugin plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender commandSender, Command command, String alias, String[] args) {
        plugin.getLogger().info("onCommand Reached in ${artifactId}");

        if (command.getName().equalsIgnoreCase("command")) {
            plugin.getLogger().info("command used");
            //do something
            return true;
        }
        return false;
    }
}
