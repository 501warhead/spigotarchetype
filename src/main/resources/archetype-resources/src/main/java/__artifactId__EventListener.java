package ${package};

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

public class ${artifactId}EventListener implements Listener{

    private final Plugin plugin;

    public ${artifactId}EventListener(Plugin plugin) {
        this.plugin = plugin;
    }

    // This is just one possible event you can hook.
    // See https://hub.spigotmc.org/javadocs/bukkit/ for a full event list.

    // All event handlers must be marked with the @EventHandler annotation
    // The method name does not matter, only the type of the event parameter
    // is used to distinguish what is handled.

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Bukkit.getServer().broadcastMessage("Player " + event.getPlayer().getName() + " placed " + event.getBlock().getType() + " at " + event.getBlock().getLocation());
    }
}
