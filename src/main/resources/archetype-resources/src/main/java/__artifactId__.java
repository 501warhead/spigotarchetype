package ${package};

import org.bukkit.Bukkit;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.*;

@Main
@Name("${artifactId}")
@Version("${version}")
@Author("me")
@Website("spigotmc.org")
@LogPrefix("my plugin")
@Commands({
        @Commands.Cmd(
                value="command",
                desc="A command",
                aliases = {"cmd", "comm"},
                permission="command.use",
                permissionMessage = "You cannot use the command!",
                usage = "/<command>"
        )
})
@Permissions({
        @Permissions.Perm(
                value = "command.use",
                desc = "allows you to use command",
                defaultValue = PermissionDefault.OP
        )
})
public class ${artifactId} extends JavaPlugin {

    //ClassListeners
    private ${artifactId}CommandExecutor executor = new ${artifactId}CommandExecutor(this);
    private ${artifactId}EventListener listener = new ${artifactId}EventListener(this);
    //ClassListeners

    @Override
    public void onEnable() {
        PluginManager manager = Bukkit.getPluginManager();

        // you can register multiple classes to handle events if you want
        //just call pm.registerEvents() on an instance of each class
        manager.registerEvents(listener, this);

        getCommand("command").setExecutor(executor);
    }

    @Override
    public void onDisable() {
        // add any code you want executed when your plugin is disabled
    }
}